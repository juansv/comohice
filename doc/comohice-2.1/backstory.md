[Castellano](#la-historia-detras-de-comohice)
[English](#the-backstory-of-howdidi)

# La historia detrás de comohice

## Todo empezó...

Escuchaba un podcast, como de costumbre, mientras iba al trabajo. Era el [episodio 368](<https://atareao.es/podcast/chuletas-a-golpe-de-clic-y-ayuda-para-comandos/>) de ["El Atareao"](<https://atareao.es/>). Hablaba de esas veces que tienes que usar un comando de la consola, de esos que usas muy de vez en cuando, y ya no te acuerdas de las opciones, del orden de los argumentos, etc. Y comentaba una herramienta que se había montado para hacerse pequeñas notas sobre el uso de comandos. Mientras lo escuchaba, con interés porque a qué usuario de GNU/linux no le pasó eso alguna vez, pensé: qué pena que, teniendo el history, no haya una forma de hacerte unas notas que simplemente documenten lo que hiciste una vez y quedó registrado en el history. ¿No hay una forma? ¿no podría hacer un echo con esa nota descriptiva y que history me devuelva el ejemplo de uso más la nota del echo?

Al llegar a la oficina, me envié un mail a mi correo personal con estas notas:

>*Un script que mire en history un comando (que se le entrega al script como argumento) y los “echo” cercanos (que se le entregan al script también como argumento), de tal forma que devuelva el history | grep “comando” + “echo”. La idea es que, cuando aprendas algo de un comando, hacer un echo con un comentario, por ejemplo:*

> `tar -xvf fichero.tar`  
> `echo “esto sirve para desempaquetar fichero.tar”`

>*y con el script busque “tar” “descomprimir” y me busque el ejemplo de uso de tar y el comentario del echo. O sólo el comentario, o solo el ejemplo. Pero me haga un filtrado a ejemplos de uso de comandos.*

>*Incluso que guarde las consultas realizadas en otro history particular del usuario. Por ejemplo, rastreando hacia atrás el history hasta la primera ejecución del script y guardándola en un archivo de texto.*

>*El objeto de este script es tener una herramienta de consulta de aquellas cosas que alguna vez aprendí, pero que por poco uso ya no me acuerdo.*

Y cuando volví a casa, me puse manos a la obra. Lo primero, definí el alcance del script.

## La especificación

Ya se sabe, te pones a programar, o empiezas cualquier proyecto, y no cesan las ideas. Y el proyecto muere de éxito antes de empezar, de tantas ideas que se te ocurrieron y que lo paralizan. Mejor acotar desde el principio lo que quería hacer. Porque la idea es sencilla y práctica tal y como se me ocurrió, no hace falta complicarla más. Decidí que mi script cumpliría la siguiente especificación:

- Se llamará `comohice` y tendrá su alias en inglés `howdidi`.

- Tiene que ser muy simple de manejar, porque no puede ser que hacer un `history | grep comando` se escriba más rápido y fácil que mi programa.

- Para indicar el nombre del programa que quiero buscar no hará falta indicar ninguna opción. Por ejemplo: `comohice ffmpeg` buscará todas las referencias a `ffmpeg`.

- Esta mera consulta buscará en history todas las líneas donde se mencione `ffmpeg` como los echoes que haya justo antes y después (si los hubiese).

- También se podrá buscar en los echoes, en este caso indicando la opción `-e` (de *echo*). Por ejemplo: `comohice ffmpeg -e comprimir` buscará en el history las menciones a `ffmpeg` cuyos echos anterior y/o posterior tenga el texto `comprimir`.

- Esta opción puede fucionar sola, sin el argumento principal, es decir: `comohice -e comprimir` busca la palabra `comprimir` en los echoes del history.

- Otra opción buscará en los argumentos del comando buscado. Será la opción `-a`, que significa *argumento* o *argument*. Por ejemplo: `comohice ffmpeg -a "dire straits"` busca las referencias al comando `ffmpeg` en cuyo argumento figurase el texto `"dire straits"`. Esto es útil, por ejemplo, si lo poco que te viene a la memoria es el nombre del archivo sobre el que hiciste *algo*.

- La última opción será `-s` (de *salvar*) que vuelca lo aprendido a un "history" particular del usuario. Este "history" se guarda en `.config/comohice/manual.txt`.

- La opción `-s` se puede ejecutar a la vez que el comando: `comohice ffmpeg -e comprimir -s`, pero también se puede ejecutar después de ver el resultado del comando sin `-s`, es decir:  
> `comohice ffmpeg -e comprimir`  
> `[...] resultado [...]`  
> `comohice -s`

- La búsqueda en el manual del usuario es opcional. Por defecto, comohice busca en el history. La opción para buscar sólo en el manual será `-m` de *manual*.

## Ajustes

La especificación anterior me ayudó a definir el *espíritu* del programa. Pero, obviamente, fueron surgiendo nuevas ideas que no eran incompatibles con la filosofía general del script, pero lo mejoraban bastante. Decidí añadir las siguientes mejoras:

- Las opciones `-a` y `-e` pueden admitir más de una palabra o frase, si se entregan separadas por comas.

- La opción `-s` tendrá su opuesta `-u` (de *undo*, deshacer) que deshace lo último que se salvó.

- La opción `-t` (de *total*) buscará tanto en el history como en el manual.

- El manual personalizado que se construye el usuario podrá estar alojado donde el usuario quiera. Por lo tanto, se define un archivo de configuración donde indicar la ruta al *manual de usuario*. Por defecto, la ruta al manual será `/home/usuario/.local/share/comohice/manual` y al archivo de configuración `/home/usuario/.config/comohice/comohice.conf`

- El *manual de usuario* se puede editar con `nano`, u otro editor de línea de comandos, sin más que ejecutar `comohice -m`

En septiembre de 2022, tras unos meses de uso, decido reescribir parte del código para incluir las siguientes mejoras:

- El módulo xdg sólo es compatible con python >= 3.6. Recibí noticias de usuarios que todavía utilizan versiones anteriores de python, por lo que cambié al módulo pyxdg que es compatible con python >= 3.0

- También por compatibilidad de versiones, decido cambiar el tipo de archivo de configuración de TOML a JSON. Con estos dos cambios, todo el programa queda compatible con python >= 3.0

- Un usuario me preguntó si `comohice` permite ejecutar algún comando pasado, como se puede hacer con `history`. No lo había previsto e incluir esta modificación es muy sencilla: basta con numerar el resultado de una consulta con `comohice` de la misma forma que hace `history`.

- La modificación anterior me inspiró para cambiar el comportamiento de la opción de salvar. A partir de la versión 2.0 `comohice` salva en el manual del usuario sólo la línea que se le indique. Si esa línea lleva `echoes` asociados, los salva también, sin necesidad que el usuario le indique nada.

- Por último, en el archivo de configuración se añade un nuevo parámetro `only_contiguous`. Este parámetro modifica el comportamiento de la búsqueda de `echoes`. Si `only_contiguous` es `True`, `comohice` sólo buscará `echoes` que estén pegados al comando en cuestión. Si `only_contiguous` es `False`, `comohice` buscará `echoes` en las líneas cercanas al comando en cuestión, no sólo en las líneas inmediatamente inferior y superior.

## Agradecimientos

Ya lo dije al principio. Esta idea no salió de la nada, me la inspiró "El Atareao". Es un momento tan bueno como otro para agradecer el trabajo de divulgación de Lorenzo "El Ataerao".

Agradecimientos a la gente que me da feedback y me ayuda a mejorar el programa.

# The backstory of howdidi

## Everything started...

I listen podcasts when I'm going job. It was the [368th episode](<https://atareao.es/podcast/chuletas-a-golpe-de-clic-y-ayuda-para-comandos/>) of ["El Atareao"](<https://atareao.es/>). He was talking of these times you have to use a command, one of those you seldom use, and don't remember the options, the arguments order, etc. And he was explaining a new tool he had developed to take notes about command use. I was listening very interested because it had happened to me too. But at the same time I thought: if there was a way to make some notes on bash history to describe what the command does, it could be enough. And then, I realized I could make that notes by typing an echo.

When I arrived to my office, I sent an email to myself with this comment:

>*An script that searches for a command in bash history and for the echoes close to the command. Similar to history | grep command + echo. This way, every time I do something new with a command, I only have to echo a little description. Example:*

>`tar -xvf filename.tar`  
>`echo "I have extracted filename.tar"`

>*Then, I tell the script look up "tar" and "extract" and it searches both, the tar line and the echo line on history but not all cases, only the cases they are close to each other. Or it searches only the echo line, or only the tar line.*

>*Even, it could save this search into another "history". In order to make a user manual with real examples, refining the bash history.*

>*This script aims to be a reminder tool, a place to note those things I once learnt about command-line.*

When I came home, I got to work on it. First, I established the scope.

## Specifications

Every time you start a project, a bunch of new ideas come to you mind. But it could kill your project because many ideas could paralyses it all. So, it is better set limits from the very first time. My original idea was simple and practice as it was, there is no need to improve it with many options that complicate everything. I decided my script would fulfill these specifications:

- It is named `comohice` in spanish and `howdidi` in english

- It should be so easy to type. If typing `history | grep command` is easier, there is no sense to do anything else.

- The name of the command you search for should be typed directly, without options. For example, `howdidi ffmpeg` searches for every mention of `ffmpeg` in bash history.

- But it also searches for `ffmpeg` and the echoes just before and after, if they exist.

- It is able to search in the echo by typing the option `-e`. For example: `howdidi ffmpeg -e compress` searches in bash history for `ffmpeg` mentions which has an echo, before or after, with the word `compress` within.

- The option `-e` could work alone. That means `howdidi -e compress` search for echoes which have `compress` within.

- The option `-a` searches in the command argument. For example: `howdidi ffmpeg -a "dire straits"` searches for each mention to `ffmpeg` in which arguments it is said `"dire straits"` anywhere. It is made for those cases you only remember the name of the file, so you can use it as a searching key.

- The last option is `-s`. It writes the result of the search in another file which will be the "user manual". This manual will be at `.config/comohice/manual.txt`.

- The `-s` option could be run at the same time of other options, like `howdidi ffmpeg -e compress -s` or could be run after seeing the search result, like this:
> `howdidi ffmpeg -e compress`  
> `[...] results [...]`  
> `howdidi -s`

- The script could also search on the user manual, but it is optional, by typing `-m`.

## Tune up

Previous specifications define "the spirit" of howdidi. But, obviously, there are some options which toke their final shape while I was making the code. Finally, I decided to include these changes:

- Options `-a` and `-e` will treat comma as words separating.

- Option `-s` will have their opposite action `-u` (undo) which remove the last saved.

- New option `-t` (total search) will search in both bash history and user manual.

- User manual would be placed wherever the user want. The path to user manual is stored in a new config file. By default, the path to user manual will be `/home/user/.local/share/comohice/manual` and the config file `/home/usuario/.config/comohice/comohice.conf`

- User manual could be edited with `nano`, or another command-line text editor, just by typing `howdidi -m`

In September 2022, after some months of using, I took the decision of re-write part of the code. Now it includes:

- Module xdg is only compatible with python >= 3.6. Some user told me they still use older python versions. I decided remove xdg and use pyxdg, which is compatible with python >= 3.0

- For the same compatibility reason, the config file moves from TOML to JSON format. The last two modifications make `howdidi` fully compatible with python >= 3.0

- I was asked if `howdidi` lets the user run a past command, as `history` does. Include this feature is so easy: From now on, `howdidi` count off the list of commands as `history` does.

- The previous improvement impelled me to change the behavior of save option. From now on, `howdidi` only saves one command, the one that fits with the line number the user should give. If any `echoes` were linked to the command, they will be saved too.

- There is a new parameter in the config file called `only_contiguous`. It modifies `howdidi` echoes searches. If `only_contiguous` is `True` then `howdidi` search for `echoes` just before or after the command. If `only_contiguous` is `False` then search for `echoes` close to the command, but not only stuck to the command.

## Acknowledgements

It was told at the beginning. The idea of `howdidi` does not appear from nothing. It was inspired by "El Atareao". Now it's the moment to thank Lorenzo "El Atareao" for his job and all the knowledge he shares.

Thanks to the people who give feedback and help me to improve `howdidi`.
