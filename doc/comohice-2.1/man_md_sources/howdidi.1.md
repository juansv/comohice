% COMOHICE(1) comohice 2.1.0
% Juan Antonio Silva Villar
% October 2022

# NAME
howdidi - search in .bash_history easier than history

# SYNOPSIS
**howdidi** [COMMAND][-a][ARGUMENT][-e][TEXT][-s][NUMBER][-u][-mt]

# DESCRIPTION
**howdidi** is the english alias for **comohice** command.

**howdidi** searches in your .bash_history in a easier and direct way than history command. Simply type the **COMMAND** you are looking for and get something similar as **history | grep COMMAND**.

There is no sense in using **howdidi** to search within the current **bash** session. All commands executed using **bash** do not be registered in .bash_history until you close your **bash** session or run **history -w**. Therefore, **howdidi** scope is the search for commands executed many time ago.

**howdidi** countes off the output of what you search in .bash_history as **history** does, so you can run whatever of those commands again by typing **!NUMBER** (where **NUMBER** is the line number **howdidi** showed).

**howdidi** has the feature of saving the search you made over .bash_history in a separate file, so that you could make your own **user manual** about commands use.

Another feature of **howdidi** is the chance of adding comments about the command you have just run using **bash**. You only have to type your comments doing **echo** before or after the execution of the command. Later on, when you search for that command using **howdidi**, we will get the entries of the command and the **echo** typed just before and after. When we save in your **user manual** using **howdidi**, those **echo** were save also automatically.

# OPTIONS
**-h**, **--help**
: Shows a basic help message.

**-a**, **--arg ARGUMENT**
: Searches for **ARGUMENT** inside .bash_history. It searches for COMMAND plus **ARGUMENT** if both were given.

**-e**, **--echo TEXT**
: Searches inside .bash_history for echoes with **TEXT** typed in. Then shows the echoes and COMMAND related. If COMMAND plus **--echo TEXT** is given, it focuses the search only in COMMAND with the echoes that contents **TEXT**.

**-s**, **--salvar NUMBER**
: Saves in **user manual** the line **NUMBER**. **howdidi** shows the output what you search numbered as **history** does.

**-u**, **--deshacer**
: Deletes the last search from the user manual (**undo** the previous **-s**).

**-m**, **--manual**
: Searches in user manual instead of .bash_history. If no other option but **-m** is given, it opens user manual with the text editor that figures in config file. It edits with nano by default.

**-t**, **--total**
: Searches inside user manual and inside .bash_history also.

# EXAMPLES
**howdidi ffmpeg -a "deep purple"**
: Searches inside .bash_history for all entries which combine **ffmpeg** and **"deep purple"** string as argument (of **ffmpeg**).

**howdidi ffmpeg -a "deep, purple"**
: Searches inside .bash_history for all entries which combine **ffmpeg** and words **"deep"** and/or **"purple"**.

**howdidi cp -a ./important-files**
: Searches inside .bash_history for all mentions to **cp** command which use **"./important-files"** among their arguments.

**howdidi -a update**
: Searches inside .bash_history for all mentions to whatever command which use **"update"** among their arguments.

**howdidi -a "update, upgrade"**
: Searches inside .bash_history for all mentions to whatever command which use **"update"** and/or **upgrade** among their arguments.

**howdidi ffmpeg -e "compress into mp3"**
: If you echo  **"compress into mp3"** just before or after execute **ffmpeg**, it searches inside .bash_history any mention to **ffmpeg** which has this echo in a contiguous line. No other mention to **ffmpeg** will be shown.

**howdidi ffmpeg -e "compress into mp3" -s**
: The same as the previous example, but it saves the searched into user manual.

**howdidi -s 1312**
: After doing one search using **howdidi**, it saves in the **user manual** the line **1312** showed in that search. If there are any echo close to line **1312**, they will be save too.

**howdidi ffmpeg -m**
: Searches inside user manual for all mentions to **ffmpeg** command.

**howdidi ffmpeg -t**
: Searches for all mentions to **ffmpeg** command in both user manual and .bash_history.

**howdidi -m**
: Opens user manual with command-line text editor (nano by default).

# FILES
*./config/comohice/comohice.json*
: Config file for **howdidi**. The path uses the original Spanish name **comohice**. This config file keeps the path to user manual, among other things. It is a **JSON** file type.

*./local/share/comohice/manual*
: File for user manual. The path uses the original Spanish name **comohice**. User manual is created in *./local/share/comohice/manual* by default, but you can change it by typing new path in *./config/comohice/comohice.json*

# BUGS
I don't hope so...

# COPYRIGHT

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org). License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>. This is free software: you are free to change and redistribute it. There is NO WARRANTY, to the extent permitted by law.

