% COMOHICE(1) comohice 2.1.0
% Juan Antonio Silva Villar
% Octubre 2022

# NOMBRE
comohice - busca en .bash_history de forma más sencilla que history

# SINOPSIS
**comohice** [COMANDO][-a][ARGUMENTO][-e][TEXTO][-s][NÚMERO][-u][-mt]

# DESCRIPCIÓN
**comohice** busca en tu .bash_history de forma más sencilla y directa que el comando history. Simplemente se le indica el **COMANDO** que queremos buscar y hace el equivalente a un **history | grep COMANDO**

No tiene sentido usar **comohice** para buscar entre los comandos ejecutados en la sesión de **bash** en curso. Los comandos ejecutados en una sesión de **bash** no se consolidan en .bash_history hasta que no se cierra sesión o se ejecuta **history -w**. Por lo tanto, **comohice** está enfocado en la búsqueda de comandos ejecutados mucho tiempo atrás.

**comohice** numera los resultados de su búsqueda en el .bash_history tal y como hace el comando **history**. Por lo tanto, es posible ejecutar alguno de los comandos que devuelve **comohice** tecleando **!NÚMERO** (donde **NÚMERO** es el número de linea).

**comohice** incluye la funcionalidad de salvar las búsquedas en .bash_history en un archivo aparte, de tal forma que el usuario pueda hacerse su propio **manual** de uso de comandos.

Otra funcionalidad que incluye **comohice** es la posibilidad de añadir comentarios sobre el comando que se acaba de ejecutar en la sesión de **bash**. Sólo hay que hacer uno o varios **echo**, antes o después de la ejecución del comando, con los comentarios que se quieren registrar. Cuando usemos **comohice** para buscar, nos devolverá tanto las coincidencias con el comando buscado como los **echo** que se teclearon antes y después. Cuando salvamos una búsqueda de **comohice** en nuestro manual, se salvan también estos **echo** de manera automática.

# OPCIONES
**-h**, **--help**
: Muestra un mensaje de ayuda básica.

**-a**, **--arg ARGUMENTO**
: Busca en el .bash_history el COMANDO que se haya ejecutado con el **ARGUMENTO**.

**-e**, **--echo TEXTO**
: Busca en el .bash_history los echo que contengan el **TEXTO** y muestra los echo y los comandos relacionados. Si se acompaña de un COMANDO centra la búsqueda en los COMANDOS cuyo echo adyacente contenga el **TEXTO**.

**-s**, **--salvar NÚMERO**
: Salva en el manual el **NÚMERO** de línea indicada. **comohice** devuelve el resultado de una búsqueda en .bash_history con cada elemento numerado, tal y como hace el comando **history**.

**-u**, **--deshacer**
: Borra la última búsqueda que hayamos salvado en el manual.

**-m**, **--manual**
: Busca sólo en el **manual**. Si se ejecuta sin ningún otro argumento, abre el manual con el editor de texto que se indique en el archivo de configuración. Por defecto, el editor es nano.

**-t**, **--total**
: Búsqueda **total**: busca tanto en .bash_history como en el manual.

# EJEMPLOS
**comohice ffmpeg -a "deep purple"**
: Busca en .bash_history todas las referencias al comando **ffmpeg** que utilizasen en sus argumentos la cadena **"deep purple"**.

**comohice ffmpeg -a "deep, purple"**
: Busca en .bash_history todas las referencias al comando **ffmpeg** que utilizasen en sus argumentos la palabra **"deep"** y/o la palabra **"purple"**.

**comohice cp -a ./documentos-importantes**
: Busca en .bash_history todas las referencias al comando **cp** que utilizasen en sus argumentos la cadena **"./documentos-importantes"**.

**comohice -a update**
: Busca en .bash_history todas las referencias a cualquier comando que entre sus argumentos tuviese la cadena **"update"**.

**comohice -a "update, upgrade"**
: Busca en .bash_history todas las referencias a cualquier comando que entre sus argumentos tuviese el texto **"update"** y/o el texto **"upgrade"**.

**comohice ffmpeg -e "comprimir mp3"**
: Si al ejecutar el comando **ffmpeg**, acto seguido ejecuté un **echo** donde escribí la cadena **"comprimir mp3"**, busca en .bash_history todas las referencias al comando **ffmpeg** que tengan un echo adyacente con la cadena **"comprimir mp3"**. Y sólo muestra estas referencias del comando **ffmpeg**, las que tengan ese echo cercano.

**comohice -s 1312**
: Tras ejecutar una búsqueda con **comohice**, salva en el manual la línea **1312** mostrada en la búsqueda anterior. Si existen algún echo adyacente a la línea, lo salva también en el manual.

**comohice ffmpeg -m**
: Busca todas las referencias al comando **ffmpeg** que haya salvado en el manual. Sólo busca en el manual. Las búsquedas en el manual no están numeradas, ya que no son registros del .bash_history

**comohice ffmpeg -t**
: Busca todas las referencias al comando **ffmepg** tanto en el .bash_history como en el manual.

**comohice -m**
: Abre el manual con un editor de texto de línea de comandos (por defecto usa nano).

# ARCHIVOS
*./config/comohice/comohice.json*
: Archivo de configuración de **comohice**. Es un archivo en formato **JSON**. Este archivo se puede editar para cambiar la ubicación del **manual** donde queramos salvar nuestras búsquedas realizadas con **comohice**.

*./local/share/comohice/manual*
: Archivo del **manual** realizado a partir de las búsquedas con **comohice**. Por defecto, **comohice** crea el manual en *./local/share/comohice/manual*, salvo que se indique otra cosa en *./config/comohice/comohice.json*

# BUGS
Espero que no haya...

# COPYRIGHT

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org). Licencia GPLv3+: GNU GPL version 3 o posterior <https://gnu.org/licences/gpl.html>. Esto es software libre: puedes cambiarlo y distribuirlo libremente. No hay garantía, en la medida que lo permita la ley.

