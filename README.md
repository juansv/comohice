# howdidi

Howdidi is a simple solution for note-making about commands use by using `.bash_history` and your `bash` terminal emulator, nothing else. Notes are typing by an `echo` and `howdidi` search for them easily. For a better understanding, please read [backstory.md](./doc/comohice-2.1/backstory.md).

### Installing "howdidi"

#### Dependencies

Install the python3 interpreter

`sudo apt-get install python3`

Install needed python modules

`sudo apt-get install python3-pip`

`python3 -m pip install pyxdg`

`python3 -m pip install json`

#### "howdidi" software

Clone the repository...

`sudo apt-get install git`

`git clone https://gitlab.com/juansv/comohice.git`

...and run the installer script

`cd comohice/`

`sudo sh comohice-2.1_howdidi-2.1_install.sh`

The installer is made for Debian GNU/linux distro. If your system has a different filesystem hierarchy, change lines 17 to 23 of `comohice-2.1_howdidi-2.1_install.sh` (look at the comment `DESTINATION DIRECTORIES`).

### Use of "howdidi"

After installing `howdidi` you could read the `man` entry. It has a bunch of examples that let you understand how to use `howdidi` very fast.

But `howdidi` is only a search software. The whole thing is aimed if everytime you run a command, of which you want to take note to remember how to use it, you make that note by typing an `echo`. Then, `howdidi` be able to search for that command and the note you made (the echo) and shows both on screen.

Nevertheless, `howdidi` itself is able to search in your `.bash_history` but it is easier than the `history` command. Please, take note that the `howdidi` search area is the `.bash_history` file. All commands executed using `bash` do not be registered in `.bash_history` until you close your `bash` session or run `history -w`. Therefore, `howdidi` scope is the search for commands executed time ago.

Again, please read [backstory.md](./doc/comohice-2.1/backstory.md).

### Technical

`howdidi` is made in python. The python3 interpreter should be installed. The `json` and `pyxdg` python modules are also needed. The installation of those components is explained at [dependencies](#dependencies) section.

The very first time you run `howdidi`, a config file will be created into user's `.config`. Read the `man` entry for more info.

### Versions

#### 1.0.3

Initial version

#### 2.0.0

- Moving from toml to json for config file.
- Moving from xdg to pyxdg module.
- Shows the info counted off as `history` makes.
- To save now the line number is required as input.

#### 2.1.0

- Now it is translated using gettext and standard translation files.
- New behaviour when search only among echoes. Now it shows the searched echoes and the commands beside.
- It also looks for commands if they were executed as `sudo`.
- New installer file.

### Author and license

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>.

