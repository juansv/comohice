# comohice

Comohice es una solución simple para hacerte pequeñas anotaciones sobre el manejo de comandos de la consola usando el propio `bash_history` y tu terminal compatible con `bash`, sin más añadidos. Las anotaciones las haces con un `echo` y las búsquedas te las resuelve `comohice`. Por favor, lee [backstory.md](./doc/comohice-2.1/backstory.md), lo entenderás mejor.

### Instalación de "comohice"

#### Dependencias

Instala el interprete de python3

`sudo apt-get install python3`

Instala los módulos de python necesarios para `comohice`

`sudo apt-get install python3-pip`

`python3 -m pip install pyxdg`

`python3 -m pip install json`

#### El software "comohice"

Clona el repositorio...

`sudo apt-get install git`

`git clone https://gitlab.com/juansv/comohice.git`

...y ejecuta el script de instalación

`cd comohice/`

`sudo sh comohice-2.1_howdidi-2.1_install.sh`

El script de instalación está hecho para distribuciones Debian GNU/linux. Si la jerarquía de tu sistema de archivos es diferente, puedes editar el script `comohice-2.1_howdidi-2.1_install.sh` en las líneas 17 a 23 (es la parte del scrit encabezada con el comentario `DIRECTORIOS DE DESTINO`).

### Uso de "comohice"

Después de instalar `comohice` podrás consultar el `man`. El `man` tiene muchos ejemplos de uso que rápidamente te harán entender el manejo del programa.

Pero `comohice` es sólo un buscador. La idea se completa si, cada vez que ejecutas un comando del que quieres hacerte alguna anotación para recordar cómo se usa, realizas esa anotación mediante un `echo`. Entonces, `comohice` será capaz de buscar el comando y el echo, es decir, te mostrará en pantalla el comando y la anotación que hiciste sobre el mismo.

No obstante, `comohice` por si mismo es un potente buscador sobre el `bash_history`, más sencillo de usar que el comando `history`. Por favor, nótese que `comohice` busca en el archivo `.bash_history`; los comandos ejecutados en una sesión de `bash` no se consolidan en el archivo `.bash_history` hasta que se cierra la sesión de `bash` o se ejecuta `history -w`. Por lo tanto, `comohice` no está pensado para buscar entre los comandos ejecutados en la sesión en curso de `bash`. La aplicación de `comohice` es la búsqueda de comandos ejecutados tiempo atrás.

Nuevamente, te invito a que leas [backstory.md](./doc/comohice-2.1/backstory.md), lo entenderás mejor.

### Tecnología

El programa `comohice` está escrito en python. Necesitas tener el interprete de python3 instalado. Además, utiliza los módulos `json` y `pyxdg`. La instalación de estos componentes se explica en el apartado [dependencias](#dependencias).

La primera ejecución de `comohice` creará un archivo de configuración en el `.config` del usuario. Lee la entrada del `man` para más información.

### Versiones

#### 1.0.3

Versión inicial

#### 2.0.0

- Cambio del tipo de archivo de configuración de formato toml a formato json.
- Cambio del módulo xdg por el módulo pyxdg.
- Devolver la información numerada como `history`.
- Salvar selectivamente un registro a partir de su número de línea.

#### 2.1.0

- Se cambian el método de traducción y pasa a usarse gettext y generar archivos de traducción estándar.
- Cambio de comportamiento si sólo se busca un echo, ahora devuelve el echo buscado y los comandos cercanos en el history.
- Busca los comandos también si se ejecutaron como `sudo`.
- Nuevo archivo de instalación.

### Autor y licencia (Author and license)

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

Licencia GPLv3+: GNU GPL version 3 o posterior <https://gnu.org/licences/gpl.html>.

