#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##############################################################################
# Comohice es un script en python que simplifica la búsqueda en el history del
# bash. Equivaldría a un history | grep "elemento buscado", pero con más
# opciones. En todo caso, son opciones sencillas, para hacer búsquedas muy
# rápidas.
#
# Howdidi is a python script that simplifies the search in the bash history.
# It is similar to history | grep "element" but has more options. Anyway,
# those options are simple, to make very fast searching.
#
# Copyright (C) 2022 Juan Antonio Silva Villar <juan.silva@disroot.org>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU, tal y como está publicada
# por la Free Software Foundation, ya sea la versión 3 de la licencia o (a
# su elección) cualquier versión posterior.
#
# Este progama se distribuye con la intención de ser útil, pero SIN NINGUNA
# GARANTÍA; ni siquiera la garantía implícita de COMERCIALIZACIÓN o
# UTILIDAD PARA UN FIN PARTICULAR. Consulte la Licencia Pública General GNU
# para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General GNU junto
# con este programa. Si no es así, consulte <http://www.gnu.org/licenses/>.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
##############################################################################

# Módulos estándar
import argparse
from pathlib import Path
import gettext
from subprocess import call

# Módulos de terceros
# no hay módulos de terceros

# Módulos propios
from auxjsonfiles import AuxJsonFiles


# Configuración de las traducciones
gettext.textdomain("comohice-2.1")
gettext.bindtextdomain("comohice-2.1", "/usr/share/locale")
# Alias para las traducciones.
_ = gettext.gettext


##############################################################################
# LAS FUNCIONES
##############################################################################

def argumentos():
    # Inicio la lista de argumentos
    # Primero creo la instancia
    arglist = argparse.ArgumentParser(description=_("Busca en tu history"))
    # El primer argumento es el comando. Es un argumento posicional, luego es
    # obligatorio. Para no hacerlo obligatorio, se añaden las opciones nargs y
    # degault. Para argumentos opcionales, sólo hace falta default, pero para
    # posicionales, tiene que venir acompañado de nargs="?", que significa
    # "en caso de no haber argumento, usa default"
    arglist.add_argument("comando", type=str, nargs="?", default="",
                         help=_("Comando a buscar en tu history o en tu manual"))
    # El segundo argumento es -e, es opcional, defino un valor por defecto
    # que es un string vacío.
    arglist.add_argument("-e", "--echo", type=str, default="",
                         help=_("Texto/s que se buscará en los echoes adyacentes"))
    # El tercer argumento es -a, es opcional, defino un valor por defecto
    # que es un string vacío.
    arglist.add_argument("-a", "--arg", type=str, default="",
                         help=_("Argumento/s del comando a buscar"))
    # Las opciones de grabar y borrar son incompatibles entre si
    incompatibles1 = arglist.add_mutually_exclusive_group()
    # El argumento -s salva el número de línea indicado de la última búsqueda.
    # El uso de -s es opcional, pero requiere de un número entero distinto de
    # cero. Aprovecho el valor "0" para indicarle al script que no quiero
    # salvar nada.
    incompatibles1.add_argument("-s", "--salvar", type=int, default=0,
                                help=_("Salva en tu manual la línea indicada"))
    # El argumento -s tiene su opuesto en -u, por defecto Falso (no borra)
    incompatibles1.add_argument("-u", "--deshacer", action="store_true", default=False,
                         help=_("Borra la última búsqueda grabada en tu manual"))
    # Las opciones de búsqueda son dos y son incompatibles entre si
    incompatibles2 = arglist.add_mutually_exclusive_group()
    # La opción -m busca sólo en el manual. Es opcional.
    incompatibles2.add_argument("-m", "--manual", action="store_true", default=False,
                         help=_("Busca sólo en el manual construido con comohice"))
    # La opción -t busca en todo a la vez (búsqueda total). Es opcional.
    incompatibles2.add_argument("-t", "--total", action="store_true", default=False,
                         help=_("Búsqueda total: history + manual"))
    # Cargamos las variables del programa
    argumento = arglist.parse_args()
    comando = argumento.comando
    echo = argumento.echo
    arg = argumento.arg
    salvar = argumento.salvar
    deshacer = argumento.deshacer
    manual = argumento.manual
    total = argumento.total
    return(comando, echo, arg, salvar, deshacer, manual, total)

def buscarcomando(comando, file):
    # Esta función recibe el contenido de un archivo (file) y busca las
    # coincidencias con comando. Devuelve los números de línea donde haya
    # coincidencia. Los devuelve en una lista de números
    lista_lineas = []
    for posicion, linea in enumerate(file):
        if linea.startswith(comando):
            lista_lineas.append(int(posicion))
        else:
            continue
    return(lista_lineas)

def buscarecho(echoes, contenidofile):
    # Esta función recibe el contenido de un archivo (file) y busca las
    # líneas que empiecen por el comando echo y contengan el string echoes que
    # recibió al ser llamada la función. El string echoes recibido puede
    # contener una lista de strings, en cuyo caso estarían separados por ","
    # El primer paso es tratar el string echo para ver si contiene uno o
    # varios strings
    echo = echoes.split(",")
    lista_lineas = []
    for posicion, linea in enumerate(contenidofile):
        # Si la línea corresponde con un comando "echo", compruebo si están
        # todos los strings recibidos por echoes
        if linea.startswith("echo"):
            # Para determinar si todos los strings están en el comando echo
            # cuento cuántos de ellos están y lo comparo con la cantidad de
            # diferentes strings que hay en echo.
            contador = 0
            for texto in echo:
                # El método find del objeto string devuelve -1 si no localiza
                # el texto buscado dentro del string
                if linea.find(texto) != -1:
                    contador = contador + 1
                else:
                    continue
            if len(echo) == contador:
                lista_lineas.append(int(posicion))
        else:
            continue
    return(lista_lineas)

def buscararg(args, contenidofile):
    # Esta función recibe el contenido de un archivo (file) y busca las
    # líneas que NO empiecen por el comando echo y contengan el string args que
    # recibió al ser llamada la función. El string arg recibido puede
    # contener una lista de strings, en cuyo caso estarían separados por ","
    # El primer paso es tratar el string args para ver si contiene uno o
    # varios strings
    arg = args.split(",")
    lista_lineas = []
    for posicion, linea in enumerate(contenidofile):
        # Si la línea no corresponde con un comando "echo", compruebo si están
        # todos los strings recibidos por args
        if not linea.startswith("echo"):
            # Para determinar si todos los strings están presentes
            # cuento cuántos de ellos están y lo comparo con la cantidad de
            # diferentes strings que hay en arg.
            contador = 0
            for texto in arg:
                # El método find del objeto string devuelve -1 si no localiza
                # el texto buscado dentro del string
                if linea.find(texto) != -1:
                    contador = contador + 1
                else:
                    continue
            if len(arg) == contador:
                lista_lineas.append(int(posicion))
        else:
            continue
    return(lista_lineas)

def adyacentes(lista_comando, lista_echoes):
    # Esta función recibe dos listas, una con los números de líneas donde
    # están los comandos y otra con los números de línea donde están los echo
    # Las compara y determina si hay un echo justo en las posiciones -1, -2 y
    # de la -2 en adelante y +1, +2 y de la +2 en adelante. Son los echoes
    # adyacentes a los comandos. Devuelve una lista con todas las posiciones
    # donde haya echo adyacentes.
    resultado = []
    for numlinea in lista_comando:
        if lista_echoes.count(numlinea -1) != 0:
            resultado.append(numlinea - 1)
        a = -2
        while lista_echoes.count(numlinea + a) != 0:
            resultado.append(numlinea + a)
            a = a - 1
        if lista_echoes.count(numlinea + 1) != 0:
            resultado.append(numlinea + 1)
        b = 2
        while lista_echoes.count(numlinea + b) != 0:
            resultado.append(numlinea + b)
            b = b + 1
    return(resultado)

def adyacentes_simple(lista_comando, lista_echoes):
    # Esta función recibe dos listas, una con los números de líneas donde
    # están los comandos y otra con los números de línea donde están los echo
    # Las compara y determina si hay un echo justo en las posiciones -1 y
    # de la -1 en adelante y +1 y de la +1 en adelante. Son los echoes
    # adyacentes a los comandos. Devuelve una lista con todas las posiciones
    # donde haya echo adyacentes.
    resultado = []
    for numlinea in lista_comando:
        a = -1
        while lista_echoes.count(numlinea + a) != 0:
            resultado.append(numlinea + a)
            a = a - 1
        b = 1
        while lista_echoes.count(numlinea + b) != 0:
            resultado.append(numlinea + b)
            b = b + 1
    return(resultado)

def alineados(lista_comando, lista_arg):
    # Esta función recibe dos listas, una con los número de líneas donde 
    # están los comandos y otra con los números de línea donde están los
    # argumentos. Las compara y selecciona las líneas de comandos que también
    # tienen los argumentos. Devuelve una lista con estas líneas seleccionadas
    resultado = []
    for numlinea in lista_comando:
        if lista_arg.count(numlinea) != 0:
            resultado.append(numlinea)
        else:
            continue
    return(resultado)

def resuelve_busqueda(comando, arg, echo, lista_comando, lista_arg, lista_echo):
    # Esta función recibe los argumentos de comohice (comando, arg y echo)
    # y las listas de comandos, argumentos y echoes que hay en los archivos
    # de manual de usuario y .bash_history (según desde que parte del programa
    # se llamen). Con esta información, resuelve la búsqueda que hace el
    # usuario de comohice y devuelve una lista con los números de líneas
    # a mostrar.

    # Al existir tres posibles argumentos de comohice, a saber, comando,
    # argumento (arg) y echo, se plantean 8 posibles combinaciones (2³ = 8)
    # Se analizan una por una

    # Primer caso: que sólo se pregunte por un comando sin argumentos ni
    # echoes. En este caso, tengo que devolver las coincidencias con el
    # comando, tengan echo cercano o no, y los echoes cercanos si los hubiese.
    # Por lo tanto, la batuta la lleva la lista de coincidencias del comando.
    if (comando != "" and arg == "" and echo == ""):
        echo_adyacente = adyacentes(lista_comando, lista_echo)
        # El resultado final es la lista de comandos más los echoes adyacentes
        resultado = lista_comando + echo_adyacente

    # Segundo caso: En este caso el comando se acompaña de un echo para
    # filtrar pero no de un argumento
    if (comando != "" and echo != "" and arg == ""):
        # Primero hago el listado de los echoes adyacentes
        echo_adyacente = adyacentes(lista_comando, lista_echo)
        # Y con esos echoes adyacentes saco sus comandos adyacentes
        comandos_adyacentes = adyacentes(echo_adyacente, lista_comando)
        # El resultado es la combinación de ambas "adyacencias"
        resultado = comandos_adyacentes + echo_adyacente

    # Tercer caso: que se pregunte por un comando y argumento, pero no por un
    # echo. En este caso, la búsqueda se debe refinar al comando acompañado
    # del argumento. Luego se añaden los echoes adyacentes.
    if (comando != "" and arg != "" and echo == ""):
        comandoarg = alineados(lista_comando, lista_arg)
        # El resultado final son los comandos con argumento y los echoes
        # adyacentes
        echo_adyacente = adyacentes(comandoarg, lista_echo)
        resultado = comandoarg + echo_adyacente

    # Cuarto caso: En este caso se pregunta por un comando, un argumento y un
    # echo. Este caso es como en anterior, pero la búsqueda se refina por los
    # echoes adyacentes. Por lo tanto:
    if (comando != "" and arg != "" and echo != ""):
        # Primero selecciono la combinación comando+argumento:
        comandoarg = alineados(lista_comando, lista_arg)
        # Y luego hago la doble adyacencia, primero de los echoes
        echo_adyacente = adyacentes(comandoarg, lista_echo)
        # Y ahora de los comando+argumento
        comandoarg_adyacente = adyacentes(echo_adyacente, comandoarg)
        resultado = comandoarg_adyacente + echo_adyacente

    # Quinto caso: Solo se pregunta por un echo, no por un comando ni un
    # argumento. En este caso, completo la lista de echo con los comandos
    # que haya una y dos líneas antes y después y se devuelve la lista de
    # echoes más la de comandos
    if (comando == "" and arg == "" and echo != ""):
        # Completo la lista de echo con los comandos que haya una y dos líneas
        # antes y después
        lista_temp = []
        for linea_echo in lista_echo:
            if linea_echo - 1 >= 0: lista_temp.append(linea_echo - 1)
            if linea_echo - 2 >= 0: lista_temp.append(linea_echo - 2)
            lista_temp.append(linea_echo + 1)
            lista_temp.append(linea_echo + 2)
        resultado = lista_echo + lista_temp

    # Sexto caso: Solo se pregunta por un argumento, no por un comando ni un
    # echo. En este caso, se devuelve la lista de argumentos, sin más
    if (comando == "" and echo == "" and arg != ""):
        resultado = lista_arg

    # Séptimo caso: Se pregunta por un argumento y por un echo. En este caso,
    # se trata como el segundo, ocupando el argumento el lugar del comando.
    if (comando == "" and echo != "" and arg != ""):
        # Primero hago la lista de echoes adyacentes
        echo_adyacente = adyacentes(lista_arg, lista_echo)
        # Luego hago la lista de los argumentos adyacentes de los echoes
        # adyacentes
        arg_adyacente = adyacentes(echo_adyacente, lista_arg)
        # El resultado final es la lista de argumentos adyacentes más los
        # echoes adyacentes
        resultado = arg_adyacente + echo_adyacente

    # Octavo caso: No se indica nada, ni comando, ni argumento ni echo.
    if (comando == "" and arg == "" and echo == ""):
        resultado = []

    return(resultado)

def resuelve_busqueda_simple(comando, arg, echo, lista_comando, lista_arg, lista_echo):
    # Esta función recibe los argumentos de comohice (comando, arg y echo)
    # y las listas de comandos, argumentos y echoes que hay en los archivos
    # de manual de usuario (también podría recibirlos del .bash_history, según
    # desde que parte del programa se llamen, pero está pensado para el
    # manual del usuario). Con esta información, resuelve la búsqueda que
    # hace el usuario de comohice y devuelve una lista con los números de
    # líneas a mostrar.

    # Al existir tres posibles argumentos de comohice, a saber, comando,
    # argumento (arg) y echo, se plantean 8 posibles combinaciones (2³ = 8)
    # Se analizan una por una

    # Primer caso: que sólo se pregunte por un comando sin argumentos ni
    # echoes. En este caso, tengo que devolver las coincidencias con el
    # comando, tengan echo cercano o no, y los echoes cercanos si los hubiese.
    # Por lo tanto, la batuta la lleva la lista de coincidencias del comando.
    if (comando != "" and arg == "" and echo == ""):
        echo_adyacente = adyacentes_simple(lista_comando, lista_echo)
        # El resultado final es la lista de comandos más los echoes adyacentes
        resultado = lista_comando + echo_adyacente

    # Segundo caso: En este caso el comando se acompaña de un echo para
    # filtrar pero no de un argumento
    if (comando != "" and echo != "" and arg == ""):
        # Primero hago el listado de los echoes adyacentes
        echo_adyacente = adyacentes_simple(lista_comando, lista_echo)
        # Y con esos echoes adyacentes saco sus comandos adyacentes
        comandos_adyacentes = adyacentes_simple(echo_adyacente, lista_comando)
        # El resultado es la combinación de ambas "adyacencias"
        resultado = comandos_adyacentes + echo_adyacente

    # Tercer caso: que se pregunte por un comando y argumento, pero no por un
    # echo. En este caso, la búsqueda se debe refinar al comando acompañado
    # del argumento. Luego se añaden los echoes adyacentes.
    if (comando != "" and arg != "" and echo == ""):
        comandoarg = alineados(lista_comando, lista_arg)
        # El resultado final son los comandos con argumento y los echoes
        # adyacentes
        echo_adyacente = adyacentes_simple(comandoarg, lista_echo)
        resultado = comandoarg + echo_adyacente

    # Cuarto caso: En este caso se pregunta por un comando, un argumento y un
    # echo. Este caso es como en anterior, pero la búsqueda se refina por los
    # echoes adyacentes. Por lo tanto:
    if (comando != "" and arg != "" and echo != ""):
        # Primero selecciono la combinación comando+argumento:
        comandoarg = alineados(lista_comando, lista_arg)
        # Y luego hago la doble adyacencia, primero de los echoes
        echo_adyacente = adyacentes_simple(comandoarg, lista_echo)
        # Y ahora de los comando+argumento
        comandoarg_adyacente = adyacentes_simple(echo_adyacente, comandoarg)
        resultado = comandoarg_adyacente + echo_adyacente

    # Quinto caso: Solo se pregunta por un echo, no por un comando ni un
    # argumento. En este caso, completo la lista de echo con los comandos
    # que haya una línea antes y después y se devuelve la lista de
    # echoes más la de comandos
    if (comando == "" and arg == "" and echo != ""):
        # Completo la lista de echo con los comandos que haya una línea
        # antes y después
        lista_temp = []
        for linea_echo in lista_echo:
            if linea_echo - 1 >= 0: lista_temp.append(linea_echo - 1)
            lista_temp.append(linea_echo + 1)
        resultado = lista_echo + lista_temp

    # Sexto caso: Solo se pregunta por un argumento, no por un comando ni un
    # echo. En este caso, se devuelve la lista de argumentos, sin más
    if (comando == "" and echo == "" and arg != ""):
        resultado = lista_arg

    # Séptimo caso: Se pregunta por un argumento y por un echo. En este caso,
    # se trata como el segundo, ocupando el argumento el lugar del comando.
    if (comando == "" and echo != "" and arg != ""):
        # Primero hago la lista de echoes adyacentes
        echo_adyacente = adyacentes_simple(lista_arg, lista_echo)
        # Luego hago la lista de los argumentos adyacentes de los echoes
        # adyacentes
        arg_adyacente = adyacentes_simple(echo_adyacente, lista_arg)
        # El resultado final es la lista de argumentos adyacentes más los
        # echoes adyacentes
        resultado = arg_adyacente + echo_adyacente

    # Octavo caso: No se indica nada, ni comando, ni argumento ni echo.
    if (comando == "" and arg == "" and echo == ""):
        resultado = []

    return(resultado)

def precarga():
    # Esta función genera un string formateado como un archivo YAML con los
    # datos básicos del archivo de configuración, para que se realice una
    # precarga de estos datos en dicho archivo. Es decir, esta función
    # permite dejar listo el archivo de configuración cuando se crea. Por
    # lo tanto, esta función se invoca al crear el archivo de configuración.

    # Se dejan preparada la precarga para dos archivos de configuración
    # uno en formato JSON (por defecto) y otro para formato YAML. Es necesario
    # comentar el que no se quiera usar y descomentar el otro.

# UNA VEZ TERMINADAS LAS TRADUCCIONES, MODIFICAR EL ARCHIVO DE PRECARGA
    # Precarga para archivo JSON
    precarga = """
{
  "title": "Config file for comohice/howdidi",
  "text_editor": "nano",
  "last_search": [],
  "only_contiguous": false
}
    """
    return(precarga)

##############################################################################
# EL PROGRAMA
##############################################################################

def main():
    ####################################
    # PARTE COMÚN
    ####################################
    # 
    # La parte común comprende la lectura de los argumentos, la definición
    # y precarga de las variables y listas y la preparación del archivo 
    # de "manual de usuario". También la verificación del archivo de history
    # Todo esto pasa por la creación (si no exisitiese) del archivo de
    # configuración y el archivo de manual.

    # LECTURA DE ARGUMENTOS Y PREPARACIÓN DE VARIABLES
    # Defino las variables y listas a utilizar. Las variables son los
    # argumentos que se entregan a comohice. Estas las puedo cargar ya
    comando, echo, arg, salvar, deshacer, manual, total = argumentos()

    # PREPARACIÓN DE LOS LISTADOS NECESARIOS
    # Y las listas son los listados de números de línea donde se mencionan
    # los comandos, echo y argumentos que definen las variables anteriores.
    # Inicializo las listas para que, cuando se mencionen, existan y el
    # programa no rompa. Pero las inicializo vacías.
    lista_comando_history = []
    lista_sudo_comando_history = []
    lista_echo_history = []
    lista_arg_history = []
    lista_comando_manual = []
    lista_echo_manual = []
    lista_arg_manual = []

    # PREPARACIÓN DEL MANUAL Y EL .BASH_HISTORY
    # Toda la información está en el archivo de configuración, por lo 
    # que el archivo de configuración se crea desde el primer momento
    # que se ejecuta comohice.
    APP = "comohice"

    # Conmutar la instancia de la clase según se quiera crear
    # archivos de configuración JSON
    CONFIG = "comohice.json"
    configfile = AuxJsonFiles(APP, CONFIG, configfile=True)

    # Los métodos de auxyamlfiles y auxjsonfiles tienen los mismos nombres
    # por lo que el uso de uno u otro tipo de archivos sólo hay que
    # determinarlo al cargar el módulo y crear la instancia, posteriormente
    # el objeto creado se maneja igual ya sea un archivo JSON o YAML.
    configfile.userauxfile()

    # Cargo algunos datos previos en el archivo de configuración. Son los
    # textos de los distintos mensajes de comohice. Pero sólo lo hago
    # si es la primera vez, es decir, si el archivo de configuración no
    # tuviese la "precarga". Utilizo el "title" como prueba de si es o no
    # la primera pre-carga.
    try:
        configfile.readone(["title"])
    except:
        # Si me devuelve alguna excepción, entonces no existe "title".
        # En ese caso, cargo por primera vez el contenido del archivo
        # de configuración.
        # Primero genero un string con toda esta información formateada
        # en JSON/YAML, cosa que hago con la función "precarga" que devuelve
        # dicho string.
        precargaconfigfile = precarga()
        # Y ahora escribo sobre el archivo de configuración.
        configfile.writeall(precargaconfigfile)
        # Mensaje fuera de la lista de mensajes:
        print(_("Creado el archivo de configuración en ~./config"))

    # El archivo del manual se crea también desde el
    # primer momento. Sin embargo, el usuario puede decidir cambiar su
    # ubicación y nombre sin más que editar el archivo de configuración y 
    # crear el archivo en la nueva ubicación (comohice no crea el manual
    # en otra ubiación que no sea ~./local/share)
    try:
        configfile.readone(["manual_file"])
    except:
        # Si me devuelve alguna excepción, entonces no existe "manual_file".
        # Creo el archivo de manual y registro la ruta en el archivo de
        # configuración
        MANUAL = "manual"
        # La ubicación de usuario de los archivos de datos es un atributo
        # de auxtomlfile. Lo aprovecho para construir el path del manual
        manualpath = Path(configfile.userdatadir / APP)
        if not manualpath.exists():
            call(['mkdir', manualpath])
        # y creo el archivo de manual
        manualfile = Path(configfile.userdatadir / APP / MANUAL)
        if not manualfile.exists():
            call(['touch', manualfile])
        # y lo registro en el archivo de configuración. OJO, para registrar
        # la ruta, primero hay que convertirla en str. Parece que los
        # diccionarios no admiten un valor tipo pathlib.PosixPath, que es
        # lo que devuelve el método Path
        configfile.writeone(["manual_file"], str(manualfile), new=True)
        print(_("Creado el manual de usuario en ~./local/share"))
    # Ya tengo una variable con la ruta al archivo que contiene el manual
    manualfile = Path(configfile.readone(["manual_file"]))

    # Repito para el .bash_history
    try:
        configfile.readone(["bash_history"])
    except:
        # Si me devuelve alguna excepción, entonces la ruta a "bash_history"
        # no está registrada en el archivo de configuración.
        # Por defecto, considera la ruta /home/usuario/.bash_history
        historyfile = Path(Path.home() / ".bash_history")
        configfile.writeone(["bash_history"], str(historyfile), new=True)
    # Ya tengo una variable con la ruta al archivo .bash_history
    historyfile = Path(configfile.readone(["bash_history"]))

    # VARIABLE POR SI SÓLO SE QUIERE SALVAR UNA BÚSQUEDA, BORRAR UNA BÚSQUEDA
    # O EDITAR EL MANUAL
    # Puede que el usuario sólo quiera salvar o borrar la última búsqueda,
    # o editar el manual, pero no hacer una búsqueda nueva, ojo:
    if (comando == "" and echo == "" and arg == ""):
        # Si no se dan argumentos de búsqueda, no tiene sentido indicar
        # la búsqueda "total", se sobre-escribe la variable, por si se
        # hubiese pedido.
        total = False
        if (salvar or deshacer) and not manual:
            solosalvar = True
            editar = False
        elif manual and not salvar and not deshacer:
            solosalvar = False
            editar = True
        else:
            # Si no se pide ni solo salvar, ni deshacer, ni editar, no se
            # pide hacer nada... mensaje cómico y reseteo todas las variables
            # para que el script termine sin hacer nada.
            # Mensaje 1
            print(_("comohice no es capaz de leer la mente"))
            solosalvar = False
            editar = False
    else:
        editar = False
        solosalvar = False
        # No puede pedirse borrar la última búsqueda si se está pidiendo
        # alguna otra búsqueda a la vez (comando, echo o arg distinto de "")
        deshacer = False


    ####################################
    # PARTE DEL .BASH_HISTORY
    ####################################
    #
    # La parte del .bash_history se refiere a toda la búsqueda en
    # .bash_history y mostrar el resultado de esta búsqueda en pantalla, en
    # el caso que el usuario haya querido buscar en .bash_history (se sabe
    # porque el usuario busca algo sin especificar que quiere buscar sólo
    # en el manual, es decir, manual=True)

    if not manual and not solosalvar and not deshacer and not editar:
        # Lo primero es crear listas con los números de línea de .bash_history
        # donde se mencionen los comandos, echoes y argumentos que el usuario
        # quiere buscar.

        # Compruebo que existe el archivo .bash_history
        if not historyfile.exists():
            # Mensaje 2
            print(_("No se encuentra su .bash_history"))
        else:
            # Abro el .bash_history, leo todas las líneas y cierro
            with open(historyfile, "r") as file:
                contenidofile = file.readlines()

            # Me hago una lista de las líneas donde se menciona el comando
            if comando != "":
                lista_comando_history = buscarcomando(comando, contenidofile)
                # Añado sudo al comando y repito la búsqueda
                sudo_comando = "sudo " + comando
                lista_sudo_comando_history = buscarcomando(sudo_comando, contenidofile)
                # uno las búsquedas del comando y sudo + comando
                lista_comando_history = lista_comando_history + lista_sudo_comando_history

            # Me hago una lista de las líneas donde se menciona el echo. En el 
            # caso de los echoes, los busco tanto si me entregan alguno para
            # buscar desde los argumentos de comohice como si no, ya que una
            # de las condiciones del programa es que devuelve el comando
            # buscado y los echoes adyacentes. Por lo tanto, lista de echoes
            # se crea siempre, sin condición.
            lista_echo_history = buscarecho(echo, contenidofile)

            # Me hago una lista de las líneas donde se menciona el argumento
            if arg != "":
                lista_arg_history = buscararg(arg, contenidofile)

            # En este punto ya tengo definidas las variables y listas,
            # procedo a realizar la búsqueda cruzando toda esta información
            # Primero miro en el archivo de configuración si el usuario
            # quiere bucear por los echoes +-1 y +-2 o sólo por los +-1
            solo_uno = configfile.readone(["only_contiguous"])
            if not solo_uno:
                resultado_history = resuelve_busqueda(comando, arg, echo,
                    lista_comando_history, lista_arg_history, lista_echo_history)
            if solo_uno:
                resultado_history = resuelve_busqueda_simple(comando, arg, echo,
                    lista_comando_history, lista_arg_history, lista_echo_history)

            # Elimino los datos repetidos y ordeno la lista
            resultado_history = list(set(resultado_history))
            resultado_history.sort()

            # Final: muestro el resultado en pantalla
            with open(historyfile, "r") as file:
                contenidofile = file.readlines()
            for indice, linea_resultado in enumerate(contenidofile):
                if resultado_history.count(indice) != 0:
                    # Sumo "1" al "indice" al imprimir por pantalla. Python
                    # recorre "indice" empezando por cero, pero las líneas
                    # del .bash_history comienzan a numerarse en 1.
                    print(indice + 1, linea_resultado.rstrip())

            # Y salvo el resultado de la búsqueda en el archivo de
            # configuración, por si luego la quiero salvar.
            configfile.writeone(["last_search"], resultado_history)


    ####################################
    # PARTE DEL MANUAL DE USUARIO
    ####################################
    #
    # La parte del manual de usuario se refiere a toda la búsqueda en
    # manual y mostrar el resultado de esta búsqueda en pantalla, en
    # el caso que el usuario haya querido buscar en su manual auto-construido.
    # Se sabe que el usuario quiere buscar en su manual porque especifica
    # manual=True o porque indica total=True

    if (manual or total) and not solosalvar and not deshacer and not editar:

        # Lo primero es crear listas con los números de línea de manual
        # donde se mencionen los comandos, echoes y argumentos que el usuario
        # quiere buscar.

        # Compruebo que existe el archivo manual
        if not manualfile.exists():
            # Mensaje 3
            print(_("No se encuentra el manual personalizado con comohice"))
        else:
            # Abro el manual, leo todas las líneas y cierro
            with open(manualfile, "r") as file:
                contenidofile = file.readlines()

            # Me hago una lista de las líneas donde se menciona el comando
            if comando != "":
                lista_comando_manual = buscarcomando(comando, contenidofile)

            # Me hago una lista de las líneas donde se menciona el echo. En el 
            # caso de los echoes, los busco tanto si me entregan alguno para
            # buscar desde los argumentos de comohice como si no, ya que una
            # de las condiciones del programa es que devuelve el comando
            # buscado y los echoes adyacentes. Por lo tanto, lista de echoes
            # se crea siempre, sin condición.
            lista_echo_manual = buscarecho(echo, contenidofile)

            # Me hago una lista de las líneas donde se menciona el argumento
            if arg != "":
                lista_arg_manual = buscararg(arg, contenidofile)

            # En este punto ya tengo definidas las variables y listas,
            # procedo a realizar la búsqueda cruzando toda esta información
            # Utilizo la función resuelve_busqueda_simple, que sólo busca
            # lo que haya en la línea justo antes o después, no busca dos
            # líneas más allá, como resuelve_busqueda, que queda reservada
            # para las búsquedas en el .bash_history, donde hay que buscar
            # con más profundidad. En el manual del usuario se supone que
            # la información está más depurada y no hay que buscar con
            # tanta profundidad.
            resultado_manual = resuelve_busqueda_simple(comando, arg, echo,
                lista_comando_manual, lista_arg_manual, lista_echo_manual)

            # Elimino los repetidos y ordeno la lista
            resultado_manual = list(set(resultado_manual))
            resultado_manual.sort()

            # Final: muestro el resultado en pantalla
            with open(manualfile, "r") as file:
                contenidofile = file.readlines()
            # Mensaje 4
            print(_("Buscando en su manual..."))
            algunresultado = False
            for indice, linea_resultado in enumerate(contenidofile):
                if resultado_manual.count(indice) != 0:
                    print(linea_resultado.rstrip())
                    algunresultado = True
            if not algunresultado:
                # Mensaje 5
                print(_("no se encuentra nada"))

    ####################################
    # PARTE RELATIVA A SALVAR/BORRAR LA BÚSQUEDA
    ####################################
    #
    # Hecho todo el trabajo, si el usuario puede querer salvar la búsqueda
    # en el manual. Tiene la opción de hacerlo durante la ejecución de la
    # búsqueda o después. Si decide hacerlo después, hay que recordar la
    # última búsqueda. Esto se hace sin más que escribir la lista en el
    # archivo de configuración. Esta escritura de datos se realiza siempre
    # que se hace una lectura por el history (ver PARTE DEL .BASH_HISTORY)
    # Por lo tanto, siempre hay que guardar la búsqueda guardada en el
    # archivo de configuración.
    # Pero antes de nada, se comprueba que haya algo que salvar o borrar.
    # Si no lo hubiese, se resetean ambas posibilidades.

    # Este método devuelve la lista de la búsqueda anterior
    resultado_history = configfile.readone(["last_search"])
    if resultado_history == []:
        # Si no hay nada que salvar o borrar, reseteo las variables de
        # salvar y borrar. Recuérdese que no se puede salvar la línea
        # "0", ya que la primera línea posible es la "1", por lo que 
        # utiliza el valor "0" para indicar que no se quiere salvar nada.
        salvar = 0
        deshacer = False

    # Antes de hacer nada, se comprueba que se esté pidiendo salvar una línea
    # válida, es decir, que esté entre las líneas de la última búsqueda. Para
    # ello se define una variable que comprueba el número de línea a salvar.
    if salvar !=0 and resultado_history != []:
        valor_valido = False
        for valor in resultado_history:
            # Los números de línea que ve el usuario por pantalla son una
            # unidad más que lo que se almacenan como búsqueda.
            if salvar == valor + 1:
                valor_valido = True
            else:
                pass
        if not valor_valido:
            # La línea que se pide salvar no corresponde con ninguna línea
            # de la última búsqueda. Se avisa al usuario y se sale
            print(_("Esta línea no corresponde con ninguna de la última búsqueda"))
        else:
            pass

    # La línea a salvar por defecto es 0, es decir, no salvar. Por lo tanto,
    # la acción de salvar tiene sentido para valores distintos de cero. Y
    # también tiene que ser un valor válido, que esté en la última búsqueda.
    if salvar != 0 and valor_valido:
        # En este punto, puedo proceder a salvar la línea de la última
        # busqueda. Para ello utilizo la función que creé arriba "adyacentes"
        # Esta función necesita dos listas para comparar, una con los
        # comandos buscados y otra con los echoes, y la función me devuelve
        # qué echoes son adyacentes a los comandos. En este caso, sólo hay 
        # un comando, por lo que una de las listas es de un único valor.
        # La otra lista, la de los echoes, hay que hacerla a partir de los
        # datos de la última búsqueda que son "echo" (empiezan por echo).
        lista_comando = [salvar - 1]
        lista_echoes = []
        origen = open(historyfile, "r")
        contenidoorigen = origen.readlines()
        for indice, linea in enumerate(contenidoorigen):
            if resultado_history.count(indice) != 0 and linea.startswith("echo"):
                lista_echoes.append(indice)
        echoes_adyacentes = adyacentes(lista_comando, lista_echoes)
        # Ahora uno la lista de ecos adyacentes con el comando y procedo a
        # salvar
        resultado_history = echoes_adyacentes + lista_comando
        # Se procede a salvar la línea indicada de la última búsqueda y sus
        # echoes adyacentes.
        origen = open(historyfile, "r")
        destino = open(manualfile, "a")
        contenidoorigen = origen.readlines()
        for indice, linea in enumerate(contenidoorigen):
            if resultado_history.count(indice) != 0:
                destino.write(linea)
        origen.close()
        destino.close()
        print(_("La última búsqueda ha sido salvada"))
        # Por último, hay que salvar la búsqueda en el archivo de
        # configuración, para darle sentido al comando deshacer.
        configfile.writeone(["last_search"], resultado_history)

    if deshacer:
        # Este método borra la última búsqueda que se salvó en el manual
        # del usuario. Lo hace contanto las líneas de la última búsqueda
        # que está registrada en el archivo de configuración y borrando
        # el mismo número de líneas al final del manual.
        # Este es el número de lineas a borrar
        numlineasborrar = len(resultado_history)
        # Abro el archivo
        with open(manualfile, "r+") as file:
            # Cargo las líneas
            todaslineas = file.readlines()
            # Determino hasta que linea se conserva (las que hay menos las
            # que quiero borrar)
            lineafinal = len(todaslineas) - numlineasborrar
            # Me posiciono al principio del archivo
            file.seek(0)
            # Lo "borro" todo (trunco desde la linea cero en adelante)
            file.truncate()
            # Escribo todas las líneas menos las últimas
            file.writelines(todaslineas[0:lineafinal])
        # Borro el registro de la última búsqueda.
        configfile.writeone(["last_search"], [])
        print(_("La última búsqueda ha sido borrada"))

    ####################################
    # PARTE RELATIVA A EDITAR EL MANUAL
    ####################################
    #
    # Para terminar, si se lanza "comohice -m", se abre un editor de texto
    # con el manual, para que el usuario lo edite. El editor lo lee en el 
    # archivo de configuración.
    if editar:
        editor = configfile.readone(["text_editor"])
        call([editor, manualfile])

if __name__ == "__main__":
    main()
